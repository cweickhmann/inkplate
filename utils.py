#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 22:00:52 2020

@author: christian
"""

def str2hex(s):
    return "".join(["{:x}".format(ord(c)).upper() for c in s])
